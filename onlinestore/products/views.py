from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import Product, Manufacturer
from django.http import JsonResponse
# from django.shortcuts import render


class ProductDetailView(DetailView):
    """Documentation for ProductDetailView

    """
    model = Product
    template_name = "products/products_details.html"


class ProductListView(ListView):
    """Documentation for ProductListView

    """
    model = Product
    template_name = "products/products_list.html"


class ManufacturerDetailView(DetailView):
    """Documentation for ManufacturerDetailView

    """
    model = Manufacturer
    template_name = "products/manufacturer_detail.html"


def product_list(request):
    products = Product.objects.all()  # [:30]
    data = {"products": list(products.values())}  # "pk", "name", "price"
    response = JsonResponse(data)
    return response


def product_detail(request, pk):
    try:
        product = Product.objects.get(pk=pk)
        data = {
            "product": {
                "name": product.name,
                "manufacturer": product.manufacturer.name,
                "description": product.description,
                "price": product.price,
                "shipping_cost": product.shipping_cost,
                "quantity": product.quantity,
                "photo": product.photo.url
            }
        }
        response = JsonResponse(data)
    except Product.DoesNotExist:
        data = {
            "error": {
                "code": 404,
                "message": "Product Does Not Exist"
            }
        }
        response = JsonResponse(data, status=404)
    return response


def manufacturer_list(request):
    manufacturer = Manufacturer.objects.filter(active=True)  # [:30]
    data = {"products": list(manufacturer.values())}  # "pk", "name", "price"
    response = JsonResponse(data)
    return response


def manufacturer_detail(request, pk):
    try:
        manufacturer = Manufacturer.objects.get(pk=pk)
        data = {
            "manufacturer": {
                "name": manufacturer.name,
                "location": manufacturer.location,
            }
        }
        response = JsonResponse(data)
    except manufacturer.DoesNotExist:
        data = {
            "error": {
                "code": 404,
                "message": "Manufacturer Not Found."
            }
        }
        response = JsonResponse(data, status=404)
    return response
