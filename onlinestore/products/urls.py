from django.urls import path
from .views import (
    ProductDetailView,
    ProductListView,
    ManufacturerDetailView,
    product_list,
    product_detail,
    manufacturer_list,
    manufacturer_detail)


urlpatterns = [
    path("", ProductListView.as_view(), name="product-list"),
    path(
        "product/<int:pk>/",
        ProductDetailView.as_view(),
        name="product-detail"
    ),
    path(
        "manufacturer/<int:pk>/",
        ManufacturerDetailView.as_view(),
        name="manufecturer"
    ),
    path("api/products/", product_list, name="product-list-api"),
    path(
        "api/products/<int:pk>/",
        product_detail,
        name="product-detail-api"
    ),
    path("api/manufacturer/", manufacturer_list, name="manufacturer-list-api"),
    path(
        "api/manufacturer/<int:pk>/",
        manufacturer_detail,
        name="manufacturer-detail-api"
    ),
]
